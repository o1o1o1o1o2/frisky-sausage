using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class respawn : MonoBehaviour
{
    
    public Transform startCheckpoint;
    private Transform currentCheckpoint;

    public GameObject endRoundPanel;
    public TextMeshProUGUI finishText;
    public TextMeshProUGUI checkPointText;
    public Transform parent;
    private Vector3[] positions;
    private Rigidbody[] rigidbodies;
    private Vector3 dir;

    public void RestartRound()
    {
        currentCheckpoint = startCheckpoint;
        LoadState();
    }

    void SaveState()
    {
        rigidbodies = parent.GetComponentsInChildren<Rigidbody>();
        positions = new Vector3[rigidbodies.Length];
        for (int i = 0; i < rigidbodies.Length; i++)
        {
            positions[i] = rigidbodies[i].position;
        }
    }

    public void LoadState()
    {
        dir = startCheckpoint.position - currentCheckpoint.position;
        for (int i = 0; i < rigidbodies.Length; i++)
        { 
            rigidbodies[i].interpolation = RigidbodyInterpolation.None;
            rigidbodies[i].Sleep();
            rigidbodies[i].position = positions[i] - dir;
            rigidbodies[i].rotation = Quaternion.Euler(Vector3.zero);
            rigidbodies[i].interpolation = RigidbodyInterpolation.Interpolate;
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        currentCheckpoint = startCheckpoint;
        SaveState();
    }

    private string deathTag = "Death";
    private string checkPointTag = "checkPoint";
    private string finishTag = "finish";

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(deathTag)) LoadState();
        else if (other.CompareTag(checkPointTag))CheckpointReach(other);
        else if (other.CompareTag(finishTag)) Finish();
    }

    void CheckpointReach(Collider other)
    {
        if (currentCheckpoint.GetComponent<CheckPoint>().next == other.gameObject.transform)
        {
            currentCheckpoint = other.gameObject.transform;
            StartCoroutine(CheckPoint());
       }
    }

    void Finish()
    {
        endRoundPanel.SetActive(true);
        finishText.enabled = true;
        RestartRound();
    }
    
    IEnumerator CheckPoint()
    {
        checkPointText.enabled = true;
        yield return new WaitForSeconds(1f);
        checkPointText.enabled = false;
    }
}

using System;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    
    public Transform target;

    private Transform _tr;
    private float _smoothSpeed = 0.125f;
    public Vector3 offset;

    private void Start()
    {
        Application.targetFrameRate = 144;
        QualitySettings.vSyncCount = 1;
        _tr = GetComponent<Transform>();
        Time.timeScale = 1.44F;
    }

    private Vector3 _targetPosition;
    
    void LateUpdate()
    {
        _targetPosition = target.position + offset;
        _tr.position = Vector3.Lerp(_tr.position, _targetPosition, _smoothSpeed);
    }
}

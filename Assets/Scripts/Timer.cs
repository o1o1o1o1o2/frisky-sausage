using System;
using TMPro;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public TextMeshProUGUI min;
    public TextMeshProUGUI sec;
    public TextMeshProUGUI mil;
    // Start is called before the first frame update

    private float timeSinceStart;

    public void StartTimer()
    {
        timeSinceStart = Time.realtimeSinceStartup;
    }

    private string s1 = "{0:00}";
    private string s2 = "{0}";
    // Update is called once per frame
    void Update()
    {
        min.SetText(s1,Mathf.Floor((Time.realtimeSinceStartup - timeSinceStart)/60));
        sec.SetText(s1,Mathf.Floor((Time.realtimeSinceStartup - timeSinceStart)%60));
        mil.SetText(s2,Mathf.Floor((Time.realtimeSinceStartup - timeSinceStart)%1 * 10));
    }
}

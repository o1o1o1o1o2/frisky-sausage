using System.Collections.Generic;
using UnityEngine.InputSystem.EnhancedTouch;
using UnityEngine;
using UnityEngine.EventSystems;
using TouchPhase = UnityEngine.InputSystem.TouchPhase;

[DefaultExecutionOrder(-1)]
public class InputManager : Singleton<InputManager>
{

    public delegate void TouchBeginDelegate();
    public event TouchBeginDelegate TouchBeginEvent;

    public delegate void TouchProcessDelegate();
    public event TouchProcessDelegate TouchProcessEvent;

    public delegate void TouchEndedDelegate();
    public event TouchEndedDelegate TouchEndedEvent;
    

    // Start is called before the first frame update
    void Awake()
    {
        EnhancedTouchSupport.Enable();
        _uiLayerNum = LayerMask.NameToLayer("UI");
        _raysastResults = new List<RaycastResult>();
    }

    // Update is called once per frame
    void Update()
    {
        if (UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches.Count > 0)
        {
            switch (UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches[0].phase)
            {
                case TouchPhase.Began:
                    if (!IsPointerOverUIElement()) TouchBeginEvent?.Invoke();
                    break;
                case TouchPhase.Moved:
                case TouchPhase.Stationary:
                    TouchProcessEvent?.Invoke();
                    break;
                case TouchPhase.Ended:
                case TouchPhase.Canceled:
                    TouchEndedEvent?.Invoke();
                    break;
            }
        }
    }
    
    private static RaycastResult _curRaysastResult;
    private static List<RaycastResult> _raysastResults;
    private static int _uiLayerNum;
    private static bool IsPointerOverUIElement()
    {
        return IsPointerOverUIElement(GetEventSystemRaycastResults());
    }

    private static bool IsPointerOverUIElement(List<RaycastResult> eventSystemRaysastResults )
    {
        for(int index = 0;  index < eventSystemRaysastResults.Count; index ++)
        {
            _curRaysastResult = eventSystemRaysastResults [index];
            if (_curRaysastResult.gameObject.layer == _uiLayerNum)
                return true;
        }
        return false;
    }
    
    static List<RaycastResult> GetEventSystemRaycastResults()
    {
        var eventData = new PointerEventData(EventSystem.current); 
        eventData.position =  UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches[0].screenPosition;
        EventSystem.current.RaycastAll( eventData, _raysastResults );
        return _raysastResults;
    }

}

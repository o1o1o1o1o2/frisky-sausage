using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class CheckPoint : MonoBehaviour
{
    private BoxCollider _collider;
    public string checkPointTag = "checkPoint";
    public Transform previous;
    public Transform next;

    private void Awake()
    {
        _collider = GetComponent<BoxCollider>();
    }

    private void OnEnable()
    {
        _collider.isTrigger = true;
        _collider.tag = checkPointTag;
    }

}

using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class SausageController : MonoBehaviour
{
    private LineRenderer _lineRenderer;
    public Transform arrowEnd;
    public int force = 900;
    public float maxForce = 13000;
    public Rigidbody rb;
    private Transform _tr;

    private Camera _camera;

    private Vector3 _startPosition;
    private Vector3 _currentPosition;
    private Vector3 _direction;

    private bool _controlBegun;


    void Start()
    {
        _tr = transform;
        _lineRenderer = GetComponent<LineRenderer>();
        _camera = Camera.main;
        InputManager.Instance.TouchBeginEvent += OnTouchBegin;
        InputManager.Instance.TouchProcessEvent += OnTouchProcess;
        InputManager.Instance.TouchEndedEvent += OnTouchEnded;
    }

    void OnTouchBegin()
    {
   
        if (rb.velocity.magnitude >= 0.5f) return;
        
        _controlBegun = true;

        _direction = Vector3.zero;
        _startPosition = _camera.ScreenToWorldPoint(UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches[0].screenPosition);
        
        arrowEnd.position = rb.position;
        _lineRenderer.SetPosition(0, rb.position);
        _lineRenderer.SetPosition(1, rb.position);
        
        _lineRenderer.enabled = true;
        arrowEnd.gameObject.SetActive(true);
    }

    void OnTouchProcess()
    {
        if (!_controlBegun) return;

        _currentPosition = _camera.ScreenToWorldPoint(UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches[0].screenPosition);

        _direction = _startPosition - _currentPosition;
        _direction.z = 0;

        SetupArrow();
    }

    private Vector3 forceV;

    void OnTouchEnded()
    {
        if (!_controlBegun) return;
        
        forceV = _direction * force;
        _lineRenderer.enabled = false;
        rb.AddForce(Vector3.Min(forceV,new Vector3(maxForce - maxForce/3,maxForce + maxForce/4,0)));
        arrowEnd.gameObject.SetActive(false);
        _controlBegun = false;
    }

  

    private void SetupArrow()
    {
        _lineRenderer.SetPosition(0, rb.position);
        arrowEnd.position = rb.position + _direction;
        var angle = Vector3.right.Angle360(_direction.normalized);
        arrowEnd.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        _lineRenderer.SetPosition(1, arrowEnd.position);
    }

}
